
// ===============================================================================================================
// -*- C++ -*-
//
// Registry.inl - Generic registry class for dynamic object management.
//
// Copyright (c) 2010 Guilherme R. Lampert
// guilherme.ronaldo.lampert@gmail.com
//
// This code is licenced under the MIT license.
//
// This software is provided "as is" without express or implied
// warranties. You may freely copy and compile this source into
// applications you distribute provided that the copyright text
// above is included in the resulting source code.
//
// ===============================================================================================================

#ifndef REGISTRY_HPP
#define REGISTRY_HPP

#include "Utility.hpp"
#include <map>

// =======================
// Template Class Registry
// =======================

template<class Key, class T, class CT>
class Registry
{
public:

	T * Request(const Key & registry_key)
	{
		T * rez;
		typename RegMap::iterator _Ptr = m_registry_map.find(registry_key);

		if (_Ptr != m_registry_map.end())
		{
			rez = (*_Ptr).second;
		}
		else
		{
			rez = 0;
		}

		return rez;
	}

	void RegisterObject(const Key & registry_key, T * const object)
	{
		std::pair<typename RegMap::iterator, bool> rez;
		rez = m_registry_map.insert(typename RegMap::value_type(registry_key, object));

		if (!rez.second)
		{
			throw std::runtime_error(std::string("Key Collision!"));
		}
	}

	T * UnregisterObject(const Key & registry_key, bool delete_object)
	{
		T * rez = 0;
		typename RegMap::iterator _Ptr = m_registry_map.find(registry_key);

		if (_Ptr != m_registry_map.end())
		{
			if (delete_object)
			{
				delete ((*_Ptr).second);
				(*_Ptr).second = 0;
			}

			rez = (*_Ptr).second;
			m_registry_map.erase(_Ptr);
		}

		return rez;
	}

	void Clear()
	{
		if (!m_registry_map.empty())
		{
			typename RegMap::iterator _Ptr = m_registry_map.begin();
			typename RegMap::const_iterator _End = m_registry_map.end();

			while (_Ptr != _End)
			{
				delete ((*_Ptr).second);
				++_Ptr;
			}

			m_registry_map.clear();
		}
	}

	static CT * Instance()
	{
		if (m_instance == 0)
		{
			m_instance = new CT;
		}
		return m_instance;
	}

	static void Kill()
	{
		if (m_instance)
		{
			delete m_instance;
			m_instance = 0;
		}
	}

private:

	typedef std::map<Key, T *> RegMap;
	RegMap m_registry_map;
	static CT * m_instance;

protected:

	Registry() { }
	virtual ~Registry() { Clear(); }
};

template<class Key, class T, class CT> CT * Registry<Key, T, CT>::m_instance = 0;

#endif // REGISTRY_HPP
