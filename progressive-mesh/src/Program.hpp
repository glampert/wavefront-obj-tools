
// ===============================================================================================================
// -*- C++ -*-
//
// Program.hpp - Application entry point.
//
// Copyright (c) 2010 Guilherme R. Lampert
// guilherme.ronaldo.lampert@gmail.com
//
// This code is licenced under the MIT license.
//
// This software is provided "as is" without express or implied
// warranties. You may freely copy and compile this source into
// applications you distribute provided that the copyright text
// above is included in the resulting source code.
//
// ===============================================================================================================

#ifndef PROGRAM_HPP
#define PROGRAM_HPP

#include "Vector.hpp"
#include "Utility.hpp"
#include "Texture.hpp"
#include "ProgressiveMesh.hpp"
#include "WavefrontObject.hpp"
#include "Mesh.hpp"
#include <time.h>

// =========================
// Program Class (Singleton)
// =========================

class Program
{
public:

	static void Initialize(int window_width, int window_height, int argc, char * argv[]);
	static void Execute();
	static void Shutdown();

private:

	 Program() { }
	~Program() { }

	static void Render();
	static void ResizeWindow(int window_width, int window_height);
	static void KeyboardCallback(unsigned char key, int x, int y);
	static void MouseCallback(int button, int state, int x, int y);
	static void MouseMotionCallback(int x, int y);
	static void MenuCallback(int value);
	static void LoadObject(const std::string & filename);
	static void SetUpMenus();
	static int  GetClickedGroup(int x, int y);

	// Demo Program Stuff:

	struct UserObjectInfo
	{
		ProgressiveMesh * pPMesh;

		unsigned int nOriginalVertexCount;
		unsigned int nCurrentVertexCount;

		int nSelectedGroup;
		float fLoadTimeInSecs;
	};

	static int m_MenuId;
	static UserObjectInfo m_LoadedObj;

	static int m_WinId;
	static int m_window_width;
	static int m_window_height;

	static Vec2 m_LastMousePos;
	static Vec2 m_CurrentMousePos;
	static bool m_MouseMov;
	static bool m_leftButtonDown;

	static float m_AngleX;
	static float m_AngleY;
	static float m_Zoom;

	static bool m_Wireframe;
	static bool m_UI_On;

	// Scroll Bar Stuff:

	struct Rect2D
	{
		int mins[2];
		int maxs[2];
	};

	static inline bool PointIntersectRect(const Rect2D & r, const int p[2])
	{
		if ((p[0] < r.mins[0]) || (p[0] > r.maxs[0]))
		{
			return false;
		}
		if ((p[1] < r.mins[1]) || (p[1] > r.maxs[1]))
		{
			return false;
		}
		return true;
	}

	static Rect2D m_SliderButton;
	static int m_scrollBarInc;
	static int m_vertsPerPixel;
};

#endif // PROGRAM_HPP
